package santander.challenge.config.msg;

public class ErrorMsgManager {
    public static final String ERROR_MSG_INTERNAL = "Error interno de aplicacion";
    public static final String ERROR_MSG_NOT_EXISTS = "No se encontro ninguna Meet up";
    public static final String ERROR_MSG_LOGIN_ERROR = "El usuario o contraseña ingresado es invalido.";

}
