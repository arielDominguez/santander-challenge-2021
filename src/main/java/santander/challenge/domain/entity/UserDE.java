package santander.challenge.domain.entity;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
@Table(name = "Account" , schema = "dbo")
public class UserDE {
	
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private Long id;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "userDE")
	private Set<AssistantsDE> assistantsDE;

	@Column(name = "NAME")
	private String name;

	@Column(name = "LASTNAME")
	private String lastname;

	@Column(name = "AGE")
	private Integer age;

	@Column(name = "NRO_DOC")
	private String nroDocument;

	@Column(name = "ACCOUNT")
	private String acccount;

	@Column(name = "PASSWORD")
	private String password;


	public UserDE() {
		super();
	}

	public UserDE(Long id, Set<AssistantsDE> assistantsDE, String name, String lastname, Integer age, String nroDocument, String acccount, String password) {
		super();
		this.id = id;
		this.assistantsDE = assistantsDE;
		this.name = name;
		this.lastname = lastname;
		this.age = age;
		this.nroDocument = nroDocument;
		this.acccount = acccount;
		this.password = password;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Set<AssistantsDE> getAssistantsDE() {
		return assistantsDE;
	}

	public void setAssistantsDE(Set<AssistantsDE> assistantsDE) {
		this.assistantsDE = assistantsDE;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getNroDocument() {
		return nroDocument;
	}

	public void setNroDocument(String nroDocument) {
		this.nroDocument = nroDocument;
	}

	public String getAcccount() {
		return acccount;
	}

	public void setAcccount(String acccount) {
		this.acccount = acccount;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
