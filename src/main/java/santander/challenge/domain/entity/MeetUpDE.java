package santander.challenge.domain.entity;


import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
@Table(name = "MeetUp", schema = "dbo")
public class MeetUpDE {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;


	@OneToMany(fetch = FetchType.LAZY, mappedBy = "meetUpDE")
	private Set<AssistantsDE> assistants;

	private String place;

	private LocalDateTime date;

	public MeetUpDE() {
		super();
	}

	public MeetUpDE(LocalDateTime meetUpDate, String direction) {
		super();
		this.date = meetUpDate;
		this.place = direction;
	}

	public Set<AssistantsDE> getAssistants() {
		return assistants;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setAssistants(Set<AssistantsDE> assistants) {
		this.assistants = assistants;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}
}
