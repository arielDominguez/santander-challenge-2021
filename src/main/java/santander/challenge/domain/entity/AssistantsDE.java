package santander.challenge.domain.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Data
@Builder
@Entity
@Table(name = "Assistants2" , schema = "dbo")
@AllArgsConstructor
@NoArgsConstructor
public class AssistantsDE {
	
	@Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_MEET_UP")
    private MeetUpDE meetUpDE;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_USER")
    private UserDE userDE;

    @Column(name = "DATE")
    private Date dateAudit;

    @Column(name = "ATTENTED")
    private boolean attented;


}
