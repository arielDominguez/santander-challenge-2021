package santander.challenge.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import santander.challenge.domain.entity.AssistantsDE;

@Repository
public interface AssistantsRepository extends JpaRepository<AssistantsDE, Long> {
}
