package santander.challenge.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import santander.challenge.domain.entity.UserDE;

@Repository
public interface UserRepository extends JpaRepository<UserDE, Long> {

    @Query("select user from UserDE user where user.acccount = ?1")
    UserDE findByAccount(String userName);
}
