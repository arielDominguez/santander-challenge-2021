package santander.challenge.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import santander.challenge.domain.entity.MeetUpDE;

import java.util.Optional;

@Repository
public interface MeetUpRepository extends JpaRepository<MeetUpDE, Long> {

    @Query("SELECT meet FROM MeetUpDE meet join AssistantsDE assis on meet.id = assis.meetUpDE.id where meet.id = ?1")
    Optional<MeetUpDE> findByIdOnAssistants(Long idMeet);
}
