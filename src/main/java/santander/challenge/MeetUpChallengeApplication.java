package santander.challenge;

import org.springframework.boot.SpringApplication;


import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableCaching
@EnableScheduling
@ComponentScan("santander.challenge.*")
@EntityScan("santander.challenge.*")
@EnableJpaRepositories("santander.challenge.*")
@EnableAutoConfiguration
public class MeetUpChallengeApplication {

    public static void main(String[] args) {
        SpringApplication.run(MeetUpChallengeApplication.class, args);
    }

}
