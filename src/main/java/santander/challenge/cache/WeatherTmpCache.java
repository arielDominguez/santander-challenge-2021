package santander.challenge.cache;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mashape.unirest.http.exceptions.UnirestException;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import santander.challenge.api.weather.service.WeatherApiService;
import santander.challenge.api.weather.to.ApiResponseTO;
import santander.challenge.api.weather.to.WeatherResponseTO;

import javax.transaction.Transactional;
import java.text.ParseException;
import java.util.List;

@Component
@Transactional
@RequiredArgsConstructor
public class WeatherTmpCache {
	
	private final WeatherApiService weatherApiService;
	
	@Cacheable("tmpDay")
	public List<WeatherResponseTO> getTmpMeetUp(){
		try {
			return weatherApiService.getWeather();
		} catch (UnirestException e) {
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@CachePut(value = "tmpDay")
	public List<WeatherResponseTO> updateWeather(){
		return getTmpMeetUp();
	}
}
