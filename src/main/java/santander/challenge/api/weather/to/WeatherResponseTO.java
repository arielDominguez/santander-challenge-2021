package santander.challenge.api.weather.to;

import lombok.Builder;
import lombok.Data;
import java.time.LocalDate;
import java.time.LocalDateTime;


@Data
@Builder
public class WeatherResponseTO {

    private Double temperature;
    private String country;
    private String city;
    private LocalDateTime date;


}

