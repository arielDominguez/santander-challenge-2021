package santander.challenge.api.weather.to;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

@Data
@Builder
public class ApiResponseTO {

    private List<WeatherResponseTO> list;


}
