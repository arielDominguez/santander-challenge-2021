package santander.challenge.api.weather.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.springframework.stereotype.Service;
import santander.challenge.api.weather.to.WeatherResponseTO;

import java.text.ParseException;
import java.util.List;

@Service
public interface WeatherApiService {
    List<WeatherResponseTO> getWeather() throws UnirestException, JsonProcessingException, ParseException;

}
