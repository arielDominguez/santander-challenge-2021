package santander.challenge.api.weather.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import lombok.extern.java.Log;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import santander.challenge.api.weather.model.Main;
import santander.challenge.api.weather.service.WeatherApiService;
import santander.challenge.api.weather.to.WeatherResponseTO;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;


@Log
@Component
public class WeatherApiServiceImpl implements WeatherApiService {

    @Value("${rapidapi.weather.url}")
    private String url;
    @Value("${rapidapi.weather.host}")
    private String host;
    @Value("${rapidapi.weather.key}")
    private String key;


    public List<WeatherResponseTO> getWeather() throws UnirestException, JsonProcessingException, ParseException {
        HttpResponse<JsonNode> response = Unirest.get(url)
                .header("x-rapidapi-key", key)
                .header("x-rapidapi-host", host)
                .asJson();
        JSONObject responseList = response.getBody().getObject();
        return mapResponse(responseList);

    }


    private List<WeatherResponseTO> mapResponse(JSONObject responseList) throws JsonProcessingException, ParseException {
        ObjectMapper mapper = new ObjectMapper();
        List<WeatherResponseTO> ret = new ArrayList<>();
        Double tmp;
        JSONObject all;
        JSONObject cityResponse =  responseList.getJSONObject("city");
        String country = cityResponse.get("name").toString();
        String province = cityResponse.get("country").toString();

        for(int i=0; i <responseList.getJSONArray("list").length(); i++){
            all = (JSONObject) responseList.getJSONArray("list").get(i);
            tmp = mapper.readValue(all.get("main").toString(), Main.class).getTemp();
            try {
                ret.add(WeatherResponseTO.builder().temperature(Double.valueOf(tmp))
                        .country(country)
                        .city(province)
                        .date(LocalDateTime.parse(String.valueOf(all.get("dt_txt")), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")))
                        .build());
            }catch (Exception e){
                log.info("No se pudo obtener la temperatura del siguiente horario: " + all.get("dt_txt"));
            }
        }
        return ret;
    }

}
