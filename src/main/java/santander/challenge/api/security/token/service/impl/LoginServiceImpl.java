package santander.challenge.api.security.token.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Component;
import santander.challenge.api.security.token.service.LoginService;
import santander.challenge.domain.entity.UserDE;
import santander.challenge.domain.repository.UserRepository;
import santander.challenge.config.exception.UserException;
@Component
@RequiredArgsConstructor
public class LoginServiceImpl implements LoginService {
	
	private final UserRepository userRepository;
	
	private final static String  salt = "$2a$04$IwmFubEGW8Nw.majttflsO";
	
	public UserDE getUser(String username, String password) throws Exception {

		UserDE user = userRepository.findByAccount(username);
		if(user == null) {
			throw new UserException("El usuario no existe. Usuario recibido: " + username );
		}
		
		String passwordEncoded = encode(password);
		if(!user.getPassword().equals(passwordEncoded)) {
			throw new UserException("La password es incorrecta. Contraseña recibida " + password + " Contraseña codificada " + passwordEncoded);
		}
		return user;
	}
	
	
	public String encode(CharSequence rawPassword) {
		return BCrypt.hashpw(rawPassword.toString(), salt);
	}
	

}
