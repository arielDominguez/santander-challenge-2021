package santander.challenge.api.security.token.service;

import org.springframework.stereotype.Service;
import santander.challenge.domain.entity.UserDE;

@Service
public interface LoginService {

    UserDE getUser(String username, String password) throws Exception;
}
