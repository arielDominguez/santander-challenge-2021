package santander.challenge.api.security.token.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import santander.challenge.api.security.token.to.AccountCredentials;

@Api
@RequestMapping("/")
public interface LoginApi {


    @PostMapping("loginUser")
    @ApiOperation(value = "Metodo encargado de devolver el clima para una meetUp")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 500, message = "Internal Server Error"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 404, message = "Not Found")})
    ResponseEntity<AccountCredentials> login(@RequestBody AccountCredentials credentials);
}
