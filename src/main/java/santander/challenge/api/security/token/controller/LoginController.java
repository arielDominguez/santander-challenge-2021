package santander.challenge.api.security.token.controller;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import santander.challenge.api.security.token.service.LoginService;
import santander.challenge.api.security.token.to.AccountCredentials;
import santander.challenge.config.msg.ErrorMsgManager;


import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
public class LoginController implements LoginApi {

	private final LoginService userService;

	public ResponseEntity<AccountCredentials> login(@RequestBody AccountCredentials credentials) {
		AccountCredentials accountCredentials = new AccountCredentials();
		try {
			userService.getUser(credentials.getUsername(), credentials.getPassword());
			String token = getJWTToken(credentials.getUsername());
			accountCredentials.setUsername(credentials.getUsername());
			accountCredentials.setToken(token);		
			return new ResponseEntity<>(accountCredentials,HttpStatus.OK);
		}catch(Exception e) {
			accountCredentials.setUsername(ErrorMsgManager.ERROR_MSG_LOGIN_ERROR);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(accountCredentials);
		}
	}

	private String getJWTToken(String username) {
		String secretKey = "myPassword";
		List<GrantedAuthority> grantedAuthorities;
		
		if(username.contains("admin")) {
			grantedAuthorities = AuthorityUtils
					.commaSeparatedStringToAuthorityList("ROLE_ADMIN");
		}else {
			 grantedAuthorities = AuthorityUtils
					.commaSeparatedStringToAuthorityList("ROLE_USER");
		}
		String token = Jwts
				.builder()
				.setId("challengeSantander")
				.setSubject(username)
				.claim("authorities",
						grantedAuthorities.stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + 600000))
				.signWith(SignatureAlgorithm.HS512,
						secretKey.getBytes()).compact();

		return "Bearer " + token;
	}
}


