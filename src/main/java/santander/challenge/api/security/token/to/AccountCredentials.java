package santander.challenge.api.security.token.to;

import lombok.Data;

@Data
public class AccountCredentials {
	private String token;
	private String username;
	private String password;

}
