package santander.challenge.api.meetup.to;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AssistantsResponseTO {
    private Long meetUpDE;
    private Long userDE;
    private Boolean enrolled;
}
