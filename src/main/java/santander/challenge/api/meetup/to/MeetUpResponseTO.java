package santander.challenge.api.meetup.to;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MeetUpResponseTO {

    private Long idUser;
    private String place;
    private Date date;
    private Boolean attented;
}
