package santander.challenge.api.meetup.to;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AmountBeerResponseTO {
    private Long idMeetUp;
    private String city;
    private LocalDateTime date;
    private Integer amountBeer;
    private String temperature;
}
