package santander.challenge.api.meetup.service;

import org.springframework.stereotype.Service;
import santander.challenge.api.meetup.to.AmountBeerResponseTO;
import santander.challenge.api.meetup.to.AssistantsResponseTO;
import santander.challenge.api.meetup.to.MeetUpRequestTO;
import santander.challenge.api.meetup.to.MeetUpResponseTO;

@Service
public interface MeetUpService {

    AmountBeerResponseTO getAmountBeerToMeet(Long idMeet) throws Exception;
    Double getTmpMeetUpById(Long id) throws Exception;
    AssistantsResponseTO addUserToMeetUp(Long idMeetUp, Long idUser) throws Exception;
    MeetUpResponseTO createMeetUp(MeetUpRequestTO meetUp) throws Exception;
    AssistantsResponseTO registerCheckIn(Long idUser, Long idMeetUp) throws Exception;

}
