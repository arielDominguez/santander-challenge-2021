package santander.challenge.api.meetup.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import santander.challenge.api.meetup.service.MeetUpService;
import santander.challenge.api.meetup.to.AmountBeerResponseTO;
import santander.challenge.api.meetup.to.AssistantsResponseTO;
import santander.challenge.api.meetup.to.MeetUpRequestTO;
import santander.challenge.api.meetup.to.MeetUpResponseTO;
import santander.challenge.api.weather.to.WeatherResponseTO;
import santander.challenge.cache.WeatherTmpCache;
import santander.challenge.domain.entity.AssistantsDE;
import santander.challenge.domain.entity.MeetUpDE;
import santander.challenge.domain.entity.UserDE;
import santander.challenge.domain.repository.AssistantsRepository;
import santander.challenge.domain.repository.MeetUpRepository;
import santander.challenge.domain.repository.UserRepository;
import santander.challenge.config.exception.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Log
@Component
@RequiredArgsConstructor
public class MeetUpServiceImpl implements MeetUpService {

    private final MeetUpRepository meetUpRepository;
    private final UserRepository userRepository;
    private final AssistantsRepository assistantsRepository;
    private final WeatherTmpCache weatherTmpCache;

    @Value("${tmp.parameter.max}")
    private String tmpMax;

    @Value("${tmp.parameter.min}")
    private String tmpMin;

    public AmountBeerResponseTO getAmountBeerToMeet(Long idMeet) throws Exception {
        Optional<MeetUpDE> opt = meetUpRepository.findByIdOnAssistants(idMeet);
        if(opt.isPresent()) {
            MeetUpDE meetUp = opt.get();
            Double tmp = getTemperature(meetUp);
            if(tmp != null){
                Integer amountBeer = estimateAmountBeers(tmp, meetUp.getAssistants().size());
                return AmountBeerResponseTO.builder()
                        .temperature(tmp+"°C")
                        .city(meetUp.getPlace())
                        .date(meetUp.getDate())
                        .idMeetUp(idMeet)
                        .amountBeer(amountBeer).build();
            }
            else throw new MeetUpException("No se pudo obtener la temperatura de la Meet up, asegurece de que sea hoy.");
        } else throw new MeetUpException("No existe una meet up con ID "+ idMeet +" o aun no se registran usuarios en la misma");
    }

    @Override
    public Double getTmpMeetUpById(Long id) throws Exception {
        Optional<MeetUpDE> opt = meetUpRepository.findById(id);
        if(opt.isPresent())
            return getTemperature(opt.get());
        else throw new MeetUpException("EL id ingresado no se corresponde con ninguna meetup");
    }

    @Override
    public AssistantsResponseTO addUserToMeetUp(Long idMeetUp, Long idUser) throws Exception {
        Optional<MeetUpDE> opt = meetUpRepository.findById(idMeetUp);
        if(opt.isPresent()){
            Optional<UserDE> opt2 = userRepository.findById(idUser);
            if(opt2.isPresent()){
                MeetUpDE meetUp = opt.get();
                UserDE user = opt2.get();
                AssistantsDE assistants = AssistantsDE.builder().userDE(user).meetUpDE(meetUp).dateAudit(new Date()).attented(false).build();
                try {
                    assistantsRepository.save(assistants);
                    return AssistantsResponseTO.builder().meetUpDE(idMeetUp).userDE(idUser).enrolled(Boolean.TRUE).build();
                } catch (Exception e) {
                    throw new MeetUpException("No se pudo registrar el usuario con id " + idUser + " en el meet up " + idMeetUp);
                }
            }
            else throw new MeetUpException("Error: El id ingresado no se corresponde con ningun usuario.");
        }
        else throw new MeetUpException("Error: EL id ingresado no se corresponde con ninguna meetup");
    }

    @Override
    public MeetUpResponseTO createMeetUp(MeetUpRequestTO meetUp) throws Exception {
        Optional<UserDE> userOpt = userRepository.findById(meetUp.getIdUser());
        if(userOpt.isPresent()) {
            UserDE userDB = userOpt.get();
            MeetUpDE meetUpDE = new MeetUpDE(meetUp.getDate(), meetUp.getPlace());
            meetUpRepository.save(meetUpDE);

            AssistantsDE assistant = AssistantsDE.builder().userDE(userDB).meetUpDE(meetUpDE).dateAudit(new Date()).attented(false).build();
            assistantsRepository.save(assistant);
            return MeetUpResponseTO.builder().idUser(meetUp.getIdUser()).place(meetUp.getPlace()).date(new Date()).attented(true).build();

        } throw new MeetUpException("El ID ingresado no se corresponde con ningun usuario");
    }

    @Override
    public AssistantsResponseTO registerCheckIn(Long idUser, Long idMeetUp) throws Exception {
        Optional<UserDE> opt = userRepository.findById(idUser);
        AssistantsResponseTO ret = null;
        if(opt.isPresent()) {
            UserDE user = opt.get();
            for(AssistantsDE assistantsDE : user.getAssistantsDE()){
                if(assistantsDE.getMeetUpDE().getId().equals(idMeetUp)) {
                    if(assistantsDE.getDateAudit().before(new Date())){
                        assistantsDE.setAttented(true);
                        assistantsRepository.save(assistantsDE);
                        return AssistantsResponseTO.builder().meetUpDE(idMeetUp).userDE(idUser).enrolled(true).build();
                    } else  throw new MeetUpException("El ID ingresado no se corresponde con ninguna meet up. La meet up no fue creada aún");
                }
            }
        } else throw new MeetUpException("El ID ingresado no se corresponde con ningun usuario");
        return ret;
    }

    private Double getTemperature(MeetUpDE meetUp) throws Exception {
        List<WeatherResponseTO> weatherResponse = weatherTmpCache.getTmpMeetUp();
        Double temp = null;
        for(WeatherResponseTO w: weatherResponse)
            if(meetUp.getDate().compareTo(w.getDate()) == 0) {
                return w.getTemperature();
            }
        throw new Exception("No se pudo obtener la temperatura");
    }


    private Integer estimateAmountBeers(Double tmp, Integer assistants){
        Double beer;
        Double tMax = Double.valueOf(tmpMax);
        Double tMin = Double.valueOf(tmpMin);

        if(tmp >= tMax) beer = 2.0;
        else if(tMin < tmp && tmp < tMax) beer = 1.0;
             else beer = 0.75;

        return calculateAmount(assistants, beer);
    }

    //Asumo que los paquetes son de a 6 birras y redondeo para arriba la cantidad de birras.
    private Integer calculateAmount(Integer assistants, Double amountBeer){
        Double ret = amountBeer * assistants;
        return (int) Math.ceil(ret / 6);
    }

}
