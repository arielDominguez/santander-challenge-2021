package santander.challenge.api.meetup.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import santander.challenge.api.meetup.to.MeetUpRequestTO;

@Api
@RequestMapping("/")
public interface MeetUpApi {

    @GetMapping("amountBeer")
    @ApiOperation(value = "Servicio que devuelve la cantidad de birras que se requieren para la meet up")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 500, message = "Internal Server Error"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 404, message = "Not Found")})
    ResponseEntity<?> amountBeer(@RequestParam Long id);

    @GetMapping("obtainWeather")
    @ApiOperation(value = "Servicio que devuelve el clima por id de meet up")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 500, message = "Internal Server Error"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 404, message = "Not Found")})
    ResponseEntity<?> obtainWeather(@RequestParam Long id);


    @PutMapping("addUser")
    @ApiOperation(value = "Servicio que registra un usuario a una determinada meet up.")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 500, message = "Internal Server Error"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 404, message = "Not Found")})
    ResponseEntity<?> addUser(@RequestParam("idMeetUp") Long idMeetUp, @RequestParam("idUser") Long idUser);

    @PostMapping("createMeetUp")
    @ApiOperation(value = "Servicio que da de alta una meet up")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 500, message = "Internal Server Error"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 404, message = "Not Found")})
    ResponseEntity<?> createMeetUp(@RequestBody MeetUpRequestTO meetUp);

    @PutMapping("checkinMeetup")
    @ApiOperation(value = "Servicio que permite a un determinado usuario realizar check in a una meet up")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 500, message = "Internal Server Error"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 404, message = "Not Found")})
    ResponseEntity<?> registerCheckIn(@RequestParam("idUser") Long idUser,@RequestParam("idMeetUp") Long idMeetUp);

}
