package santander.challenge.api.meetup.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import santander.challenge.api.meetup.service.MeetUpService;
import santander.challenge.api.meetup.to.AmountBeerResponseTO;
import santander.challenge.api.meetup.to.AssistantsResponseTO;
import santander.challenge.api.meetup.to.MeetUpRequestTO;
import santander.challenge.api.meetup.to.MeetUpResponseTO;
import santander.challenge.config.exception.*;

@Log
@RestController
@RequiredArgsConstructor
public class MeetUpUpController implements MeetUpApi {

    private final MeetUpService meetUpService;

    public ResponseEntity<?> amountBeer(@RequestParam Long id) {
        try {
            log.info("Se esta ejecuntando el proceso para obtener la cantidad de cajones de birras");
            AmountBeerResponseTO amountBeerResponse = meetUpService.getAmountBeerToMeet(id);
            log.info("El proceso ha terminado");
            return new ResponseEntity<>(amountBeerResponse,HttpStatus.OK);
        }catch(MeetUpException e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
        catch(Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    public ResponseEntity<?> obtainWeather(@RequestParam Long id) {
        try {
            log.info("Se esta ejecuntando el proceso para obtener la temperatura");
            Double tmpMeetUpResponse = meetUpService.getTmpMeetUpById(id);
            log.info("El proceso ha terminado");
            return new ResponseEntity<>("La temperatura para la Meetup con ID " +id + " es " + tmpMeetUpResponse +"°C",HttpStatus.OK);
        }catch(MeetUpException e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }catch(Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    public ResponseEntity<?> addUser(@RequestParam("idMeetUp") Long idMeetUp, @RequestParam("idUser") Long idUser) {
        try {
            log.info("Se esta ejecuntando el proceso para registrar un usuario a una meet up en especifico.");
            AssistantsResponseTO assistantsResponseTO = meetUpService.addUserToMeetUp(idMeetUp, idUser);
            log.info("El proceso ha terminado");
            return new ResponseEntity<>(assistantsResponseTO,HttpStatus.OK);
        }catch(MeetUpException e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }catch(Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    public ResponseEntity<?> createMeetUp(@RequestBody MeetUpRequestTO meetUp) {
        try {
            log.info("Se esta ejecuntando el proceso para registrar una nueva meet up");
            MeetUpResponseTO meetUpCreated = meetUpService.createMeetUp(meetUp);
            log.info("El proceso ha terminado");
            return new ResponseEntity<>(meetUpCreated,HttpStatus.OK);
        }catch(MeetUpException e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }catch(Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    public ResponseEntity<?> registerCheckIn(@RequestParam("idUser") Long idUser,@RequestParam("idMeetUp") Long idMeetUp) {
        try {
            log.info("Se esta ejecuntando el proceso para realizar el checking de un usuario");
            AssistantsResponseTO attendanceRegistered = meetUpService.registerCheckIn(idUser, idMeetUp);
            log.info("El proceso ha finalizado");
            return new ResponseEntity<>(attendanceRegistered,HttpStatus.OK);
        }catch(MeetUpException e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }catch(Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }
}