package santander.challenge.schedule;

import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import santander.challenge.api.weather.to.WeatherResponseTO;
import santander.challenge.cache.WeatherTmpCache;

import java.util.List;

@Log
@Component
@RequiredArgsConstructor
public class TaskTmpSchedule {

        private final WeatherTmpCache weatherTmpCacheService;

        @Scheduled(initialDelay = 6000, fixedRate = 60000 * 5)
        protected void runTask() {
            log.info("Schedule : Proceso que obtiene la temperatura de Mendoza...");
            List<WeatherResponseTO> ret = weatherTmpCacheService.updateWeather();
            log.info("Temperatura: " + ret.get(0).getTemperature()+"°C");
        }


    }
