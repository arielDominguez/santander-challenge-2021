package test.mock;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mashape.unirest.http.exceptions.UnirestException;
import lombok.NoArgsConstructor;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import santander.challenge.api.meetup.service.impl.MeetUpServiceImpl;
import santander.challenge.api.meetup.to.MeetUpResponseTO;
import santander.challenge.api.weather.service.WeatherApiService;
import santander.challenge.api.weather.service.impl.WeatherApiServiceImpl;
import santander.challenge.api.weather.to.WeatherResponseTO;
import santander.challenge.cache.WeatherTmpCache;
import santander.challenge.domain.entity.AssistantsDE;
import santander.challenge.domain.entity.MeetUpDE;
import santander.challenge.domain.entity.UserDE;
import santander.challenge.domain.repository.AssistantsRepository;
import santander.challenge.domain.repository.MeetUpRepository;
import santander.challenge.domain.repository.UserRepository;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.Date;
import java.util.Optional;

import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes={test.mock.WeatherMock.class})
public class WeatherMock {

    @Mock
    private UserRepository userRepository;
    @Mock
    private MeetUpRepository meetUpRepository;
    @Mock
    private AssistantsRepository assistantsRepository;

    @Test
    public void whenCreateMeetUpReturnMeetUp() throws ParseException, UnirestException, JsonProcessingException {
        UserDE user = new UserDE(null, null,"Julian","Gomez" , 21, "38554464" , "ffffff", "pass1234");
        MeetUpDE meetUp = new MeetUpDE(LocalDateTime.of(2015, Month.JULY, 29, 19, 30, 40), "Grand Bourg");
        AssistantsDE assistantsDE = AssistantsDE.builder().meetUpDE(meetUp).userDE(user).attented(Boolean.TRUE).build();

        when(userRepository.findById(1L)).thenReturn(Optional.ofNullable(user));
        when(meetUpRepository.save(meetUp)).thenReturn(new MeetUpDE());
        when(assistantsRepository.save(assistantsDE)).thenReturn(AssistantsDE.builder().build());

        Optional<UserDE> userOpt = userRepository.findById(1L);
        if(userOpt.isPresent()) {
            UserDE userDB = userOpt.get();
            MeetUpDE meetUpDE = new MeetUpDE(meetUp.getDate(), meetUp.getPlace());
            meetUpRepository.save(meetUpDE);

            AssistantsDE assistant = AssistantsDE.builder().userDE(userDB).meetUpDE(meetUpDE).dateAudit(new Date()).attented(false).build();
            assistantsRepository.save(assistant);

        } else Mockito.doThrow(new Exception("El ID ingresado no se corresponde con ningun usuario"));

    }
}