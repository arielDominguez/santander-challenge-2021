CREATE TABLE [dbo].[Account](
                                [ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
                                [NAME] [varchar](50) NULL,
                                [LASTNAME] [varchar](50) NULL,
                                [AGE] [numeric](18, 0) NULL,
                                [NRO_DOC] [numeric](18, 0) NULL,
                                [ACCOUNT] [varchar](50) NULL,
                                [PASSWORD] [varchar](max) NULL,
                                CONSTRAINT [PK_Account] PRIMARY KEY CLUSTERED
                                    (
                                     [ID] ASC
                                        )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[meet_up](
                                [ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
                                [PLACE] [varchar](50) NULL,
                                [DATE] [datetime2](7) NULL,
                                CONSTRAINT [PK_MeetUp] PRIMARY KEY CLUSTERED
                                    (
                                     [ID] ASC
                                        )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO


CREATE TABLE [dbo].[Assistants2](
                                    [ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
                                    [ID_MEET_UP] [numeric](18, 0) NOT NULL,
                                    [ID_USER] [numeric](18, 0) NOT NULL,
                                    [DATE] [datetime2](7) NULL,
                                    [ATTENTED] [bit] NULL,
                                    CONSTRAINT [PK_Assistants2] PRIMARY KEY CLUSTERED
                                        (
                                         [ID] ASC
                                            )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Assistants2]  WITH CHECK ADD  CONSTRAINT [FK_Assistants2_Account] FOREIGN KEY([ID_USER])
    REFERENCES [dbo].[Account] ([ID])
GO

ALTER TABLE [dbo].[Assistants2] CHECK CONSTRAINT [FK_Assistants2_Account]
GO

ALTER TABLE [dbo].[Assistants2]  WITH CHECK ADD  CONSTRAINT [FK_Assistants2_meet_up] FOREIGN KEY([ID_MEET_UP])
    REFERENCES [dbo].[meet_up] ([ID])
GO

ALTER TABLE [dbo].[Assistants2] CHECK CONSTRAINT [FK_Assistants2_meet_up]
GO


