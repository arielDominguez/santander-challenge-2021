# Challenge 2021 - Birras | Santander Tecnologia
Desarrollo back end para el dasafio Meet Up. A continuacion se exponen distintos aspectos de la documentacion propia del servicio REST. Entre ellos se encuentra la configuracion de seguridad, link de acceso al Swagger de la API, tecnologias utilizadas, etc.

##Run as
Para correr la aplicacion basta con ejecutar la clase principal llamada MeetUpChallengeApplication, de la siguiente manera Run As --> Java Application

##Tecnologias utilizadas
- Java 8 y Spring boot

- Maven

- Cliente MS SQL Server, database: Santander, password: xcgh2015 (Se puede configurar en el application.properties)


##Configuracion

- La aplicacion se ejecuta en http://localhost en el puerto 8090
- La siguiente url referencia al swagger de la API - http://localhost:8090/swagger-ui.html
- Se utilizo JWT para la seguridad de aplicacion, se requiere que se introduzca en el header authentication el token obtenido. Por lo tanto es necesario generar el token de authenticacion, tal y como se expone a continuacion:

Es necesario consumir el servicio con el siguiente endpoint http://localhost:8080/logIn. Es necesario enviar en el request un usuario y contraseña en formato JSON, para obtener el token como response. Sin embargo, se deja a continuacion un token ya generado que se podria inyectar en el request:  

"token": "Bearer eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiJjaGFsbGVuZ2VTYW50YW5kZXIiLCJzdWIiOiJhZG1pbjIxIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9BRE1JTiJdLCJpYXQiOjE2MTE4NzA2MzEsImV4cCI6MTYxMTg3MTIzMX0.nAV8KDqvhm88OBLujrkARxEM4UT9_ZQ7IaD2kNQGCIF7jjECTgxLI08F4ztcfrDnmM838pkLHsWxi46kpa322A"

Cabe mencionar que para distinguir un usuario comun con un admin, basta con ver su prefijo. Por ejemplo, adminroberto (es un administrador), roberto(es un usuario comun).

## DLLs y Esquema CI/CD

En el directorio Resources se encuentra el archivo database-dll.sql quien contiene los dlls de las tablas utilizadas en un dialecto propio de MS Sql Server. Por otra parte, se incluye en el directorio api-doc una imagen con el esquema CI/CD propuesto.


## Observaciones 
- Se opto por utilizar la API https://rapidapi.com/community/api/open-weather-map, el servicio que brinda la temperatura en intervalos de 3 hs de una determinada ciudad.
- La cantidad de cervezas se redondea para arriba y se divide la cantidad por 6. Suponiendo que el package de birras es de 6 unidades.
- Se cachea la temperatura obtenida para hacer mas performante el servicio, ya que, de alguna manera la temperatura no suele cambiar bruscamente en el transcurso del tiempo.
- Se configuro un schedulo que muestra en los logs cada 5 min la temperatura de Mendoza, Argentina.